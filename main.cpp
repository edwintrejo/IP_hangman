#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>

using namespace std;

const int MAX_TRIES = 6;
int letterFill(char, string, string & );

int main() {
        string name;
        char letter;
        int wrongGuessAmount = 0;
        string word;
		char choice;
        string words[] = {
                "mercury",
                "venus",
                "earth",
                "mars",
                "jupiter",
                "saturn",
                "uranus",
                "neptune",
                "pluto"
        };
		do{
			//choose and copy a word from array of words randomly
			srand(time(NULL));
			int n = rand() % 10;
			word = words[n];

			// Initialize the secret word with the * character.
			string unknown(word.length(), '*');

			//instructions
			cout << "\n\nWelcome to hangman...Guess a planet Name";
			cout << "\n\nletters are represented by a star|*.";
			cout << "\n\nenter one letter per attempt";
			cout << "\n\nYou have " << MAX_TRIES << " tries to guess the word.";

			// Loop until the guesses are used up	
			while (wrongGuessAmount < MAX_TRIES) {
					cout << "\n\n" << unknown;
					cout << "\n\nGuess a letter: ";
					cin >> letter;
					// Fill secret word with letter if the guess is correct, or wrong++
					if (letterFill(letter, word, unknown) == 0) {
							cout << endl << "That is not a correct letter!" << endl;
							wrongGuessAmount++;
					} else {
							cout << endl << "You found a letter! Isn't that exciting!" << endl;
					}
					// Tell user how many guesses has left.
					cout << "You have " << MAX_TRIES - wrongGuessAmount;
					cout << " guesses left." << endl;
					// Check if user guessed the word.
					if (word == unknown) {
							cout << word << endl;
							cout << "Yeah! You got it!";
							break;
					}
			}
			if (wrongGuessAmount == MAX_TRIES) {
					cout << "\nSorry, you lose...you've been hanged." << endl;
					cout << "The word was : " << word << endl;
			}
		cout<<"do you want play again ? y/n ";
   		cin>>choice;
		}while(choice=='y');

		cout << "Thanks for Playing!";
        cin.ignore();
        cin.get();
        return 0;
}

/* Take a one character guess and the secret word, and fill in the
 unfinished guessword. Returns number of characters matched.
 Also, returns zero if the character is already guessed. */

int letterFill(char guess, string secretword, string & guessword) {
        int i;
        int matches = 0;
        int len = secretword.length();
        for (i = 0; i < len; i++) {
                // Did we already match this letter in a previous guess?
                if (guess == guessword[i])
                        return 0;
                // Is the guess in the secret word?
                if (guess == secretword[i]) {
                        guessword[i] = guess;
                        matches++;
                }
        }
        return matches;
}
